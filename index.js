const PokeDataProcessing = require('./src/PokeDataProcessing')
const DATA_DIR_PATH = './src/db/evolution-chain';


const pokeDP = new PokeDataProcessing(DATA_DIR_PATH);
pokeDP.createDB("./Results/pokemon-evolution-chain-db.json");
