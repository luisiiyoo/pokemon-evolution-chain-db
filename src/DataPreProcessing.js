const fs = require('fs');

class DataPreProcessing {

  getElementsPath = (dirPath) => {
    const files = fs.readdirSync(dirPath);
    files.sort((a, b) => a - b);
    return files;
  }

  readJsonFile = (file, dirPath = "./", extra = '') => {
    try {
      const fullPath = dirPath.charAt(dirPath.length - 1) === '/' ? dirPath : `${dirPath}/`;
      const fullPathFileName = fullPath + file + "/" + extra;
      const rawdata = fs.readFileSync(fullPathFileName);
      return JSON.parse(rawdata);
    } catch (err) {
      console.error(err);
    }
  }

  createJsonFile = (nameFile, data) => {
    try {
      const dictstring = JSON.stringify(data);
      fs.writeFileSync(nameFile, dictstring);
    } catch (err) {
      console.error(err);
    }
  }

  snakeToCamel = (str) => {
    const firstChar = str.charAt(0);
    const newStr = str.replace(firstChar, firstChar.toLowerCase())
      .replace(/\./g, "")
      .replace(/\s/g, "");
    return newStr.replace(
      /([-_][a-z])/g,
      (group) => group.toUpperCase()
        .replace('-', '')
        .replace('_', '')
    );
  }

  changeNamePropertiesToCamelCase = (evDet) => {
    for (const prop in evDet) {
      const snaketNameProp = this.snakeToCamel(prop);
      if (snaketNameProp !== prop) {
        evDet[snaketNameProp] = evDet[prop];
        delete evDet[prop];
      }
    }
    return evDet;
  }
}

module.exports = DataPreProcessing;