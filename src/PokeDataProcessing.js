const colors = require('colors');
const DataPreProcessing = require('./DataPreProcessing');
const POKE_DATA = require('./db/pokedex-db.json');
// const PokemonTypeColor = require('./db/pokemon-types');

const INDEX_JSON = "index.json";

class PokeDataProcessing extends DataPreProcessing {
  constructor(dirPath) {
    super();
    this.dirPath = dirPath;
  }

  createDB = (filePath) => {
    console.log('Reading evolution-chain database...'.yellow);
    const pokeChainObjs = this.readEvolutionChainData();
    // pokeChainObjs.splice(1);
    // console.log(pokeChainObjs)

    console.log('Mapping data...'.green);
    const data = this.getPokemonChainObj(pokeChainObjs);

    console.log('Creating database...'.green);
    this.createJsonFile(filePath, data);
    console.log("Done!!".blue);
    console.log(`JSON Saved: '${filePath}'`.red);
  }

  readEvolutionChainData = () => {
    const files = this.getElementsPath(this.dirPath);
    return files.map(file => {
      return this.readJsonFile(file, this.dirPath, INDEX_JSON);
    });
  }

  getEvolutions = (pokeChainObj, evolLevel = 1, level = { maxEvolLevel: 1 }) => {
    const { "evolution_details": evolDetails, "evolves_to": evolvesTo, species: { url } } = pokeChainObj;
    const idPokemon = this.getPokemonIdFromURL(url);
    const { name, type, base } = POKE_DATA[idPokemon - 1];

    // Removing/replacing properties that might not be useful or have snake case
    this.changeNamePropertiesToCamelCase(base);
    const evolDetailsClean = evolDetails.map(evDet => {
      delete evDet['trigger'];
      this.changeNamePropertiesToCamelCase(evDet);
      return evDet;
    });

    const evolvesToRecursive = evolvesTo.map((pokeEvol) => {
      const nextEvolLevel = evolLevel + 1;
      level.maxEvolLevel = nextEvolLevel > level.maxEvolLevel ? nextEvolLevel : level.maxEvolLevel;
      return this.getEvolutions(pokeEvol, nextEvolLevel, level)
    });

    return {
      id: idPokemon,
      name: name,
      type,
      base,
      evolDetails: evolDetailsClean,
      evolvesTo: evolvesToRecursive,
      evolLevel: evolLevel,
      maxEvolLevel: level.maxEvolLevel
    }

  }

  getPokemonChainObj = (chains) => {
    try {
      return chains.map(({ chain: poke, id: idChainEvol }) => {
        const pokeChain = this.getEvolutions(poke);
        return {
          idChainEvol,
          ...pokeChain
        }
      });
    } catch (err) {
      console.error(err);
    }
  }

  getPokemonIdFromURL(url) {
    const ENDPOINT = "/api/v2/pokemon-species/";
    const idStr = url.substring(ENDPOINT.length, url.length - 1)
    return Number(idStr);
  }
}

module.exports = PokeDataProcessing;